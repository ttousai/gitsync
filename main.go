package main

import (
	"bitbucket.org/stee/gitsync/cmd"
	"flag"
	"github.com/golang/glog"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"
	"strings"
)

type Data struct {
}

func main() {
	flag.Parse()
	syncdir := getHomeDir()
	glog.Info("Starting gitsync in " + syncdir)

	// setup handlers for static resources, javascript and css
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/start/", startHandler)
	http.HandleFunc("/sync/", syncHandler)
	//	http.HandleFunc("/view/", viewHandler)
	http.Handle("/view/", http.StripPrefix("/view/", http.FileServer(http.Dir(syncdir))))
	http.ListenAndServe(":8080", nil)
}

var templates = template.Must(template.ParseFiles("templates/view.html", "templates/start.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, d *Data) {
	err := templates.ExecuteTemplate(w, tmpl+".html", d)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func startHandler(w http.ResponseWriter, r *http.Request) {
	isFirstRun(true)
	http.Redirect(w, r, "/view/", http.StatusFound)
}

func syncHandler(w http.ResponseWriter, r *http.Request) {
	/*
	 * TODO: find elegant solution to figuring remote gitsync dir.
	 * Currently assumes user enters full URL to gitsync dir.
	 * Since we default to HOMEDIR/gitsync for sync'ed files.
	 * Implement other git transports (git,http[s])
	 */
	remote := r.FormValue("host")
	src := "ssh://" + remote
	dest := getHomeDir()
	glog.Infof("Synchronizing from %s to %s", src, dest)
	if err := cmd.SyncWith(src, dest); err != nil {
		glog.Error(err)
	}
	http.Redirect(w, r, "/view/", http.StatusFound)
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "view", &Data{})
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	if isFirstRun(false) {
		renderTemplate(w, "start", &Data{})
	} else {
		http.Redirect(w, r, "/view/", http.StatusFound)
	}
}

func isFirstRun(create bool) bool {
	var (
		created = false
		d       = getHomeDir()
	)
	if create {
		created = createSyncDir(d)
		if created {
			glog.Infof("First run of gitsync")
			glog.Infof("Initializing gitsync repository %s", d)
			if err := cmd.InitSyncDir(d); err != nil {
				glog.Fatal(err)
			}
			if err := cmd.Commit(); err != nil {
				glog.Fatal(err)
			}
			return true
		}
	} else {
		// TODO: handle the error from os.Stat
		f, _ := os.Open(d)
		fi, err := f.Stat()
		if err == nil {
			if fi.IsDir() {
				return false
			}
		}
		return true
	}
	return false
}

func createSyncDir(d string) bool {
	if err := os.Mkdir(d, 0755); err != nil {
		if os.IsExist(err) {
			return false
		}
		glog.Fatal(err)
	} else {
		ioutil.WriteFile(d+"/.gitsync", []byte(""), 0644)
	}
	return true
}

func getHomeDir() (d string) {
	switch runtime.GOOS {
	case "windows":
		d = os.Getenv("USERPROFILE") + "/gitsync"
		d = strings.Replace(d, "\\", "/", -1)
	case "linux":
		d = os.Getenv("HOME") + "/gitsync"
	}
	return
}
