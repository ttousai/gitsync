package cmd

import (
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"testing"
	"time"
)

// Test InitSyncDir by attempting the creation
// of a new git repository.
func TestInitSyncDir(t *testing.T) {
	d, err := ioutil.TempDir("", "gitsync")
	if err != nil {
		t.Fatal(err)
	}
	if err := InitSyncDir(d); err != nil {
		t.Error(err)
	}
	if err := os.RemoveAll(d); err != nil {
		t.Fatal(err)
	}
}

// Test SyncWith function bye creating a new repo
// and trying to clone it.
func TestSyncWith(t *testing.T) {
	d := init_repo("gitsync", t)
	c := d + "-clone"
	d_url := "file://" + d
	defer cleanup(c, d)

	if err := SyncWith(d_url, c); err != nil {
		t.Error(err)
	}
}

func TestCommit(t *testing.T) {
	d := init_repo("gitsync", t)
	defer cleanup(d)

	if err := os.Chdir(d); err != nil {
		t.Fatal(err)
	}
	create_temp_files()
	if err := Commit(); err != nil {
		t.Fatal(err)
	}
}

// Test GetChanges by initializing a new repository
// cloning it, adding new files and attempting pull.
func TestGetChanges(t *testing.T) {
	r := init_repo("gitsync", t)
	c := clone_repo(r, t)
	defer cleanup(c, r)

	if err := os.Chdir(r); err != nil {
		t.Fatal(err)
	}

	create_temp_files()
	Commit()

	if err := os.Chdir(c); err != nil {
		t.Fatal(err)
	}
	if err := GetChanges(); err != nil {
		t.Error(err)
	}
}

func TestSendChanges(t *testing.T) {
	r := init_repo("gitsync-sc", t)
	c := clone_repo(r, t)
	defer cleanup(c, r)

	if err := os.Chdir(c); err != nil {
		t.Fatal(err)
	}

	create_temp_files()
	Commit()

	if err := SendChanges(); err != nil {
		t.Error(err)
	}
}

// Create new git repository
func init_repo(s string, t *testing.T) (d string) {
	d, err := ioutil.TempDir("", s)
	if err != nil {
		t.Fatal(err)
	}

	if err := InitSyncDir(d); err != nil {
		t.Fatal(err)
	}
	return
}

// Clone a repo.
func clone_repo(d string, t *testing.T) (c string) {
	c = d + "-clone"
	d_url := "file://" + d

	if err := SyncWith(d_url, c); err != nil {
		t.Error(err)
	}
	return
}

// Create a bunch of temporary files in the current dir.
func create_temp_files() {
	MAX := 10
	// write random files in git repo.
	rand.Seed(int64(time.Now().Hour()))
	m := strconv.Itoa(rand.Intn(MAX))
	n := strconv.Itoa(rand.Intn(MAX))
	ioutil.WriteFile("file1", []byte(m), 0666)
	ioutil.WriteFile("file2", []byte(n), 0666)
}

// Remove files and directories after use.
func cleanup(d ...string) {
	for _, v := range d {
		os.RemoveAll(v)
	}
}
