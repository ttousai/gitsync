package cmd

import (
	"errors"
	"fmt"
	"github.com/golang/glog"
	"os"
	"os/exec"
	"strings"
	"time"
)

// initializes a directory as a synchronization directory.
func InitSyncDir(d string) error {

	if err := os.Chdir(d); err != nil {
		glog.Fatal(err)
	}

	_, err := exec.Command("git", "init").Output()
	if err != nil {
		return err
	}

	if !isSyncDir(d) {
		return fmt.Errorf("%s is not a sync directory", d)
	}

	setOptDenyCurrentBranch(d)
	return nil
}

// creates synchronization directory that is synchronized
// with the src directory.
func SyncWith(src string, d ...string) error {
	dest := ""
	if len(d) > 0 {
		dest = d[0]
	} else {
		t := strings.Split(src, "/")
		dest = t[len(t)-1]
	}

	_, err := exec.Command("git", "clone", src, dest).Output()
	if err != nil {
		return err
	}

	if !isSyncDir(dest) {
		return fmt.Errorf("%s is not a sync directory", dest)
	}

	setOptDenyCurrentBranch(dest)
	return nil
}

func Commit() error {
	_, err := exec.Command("git", "add", ".").Output()
	if err != nil {
		return err
	}
	_, err = exec.Command("git", "commit", "-m", time.Now().String()).Output()
	if err != nil {
		return err
	}
	return nil
}

// synchronizes a synchronzation directory
func GetChanges() error {
	_, err := exec.Command("git", "pull").Output()
	if err != nil {
		return err
	}
	return nil
}

func SendChanges() error {
	_, err := exec.Command("git", "push", "origin", "master").Output()
	if err != nil {
		return err
	}
	return nil
}

func Status() error {
	return errors.New("Not implemented!")
}

func isSyncDir(d string) bool {
	d = d + "/.git"

	finfo, err := os.Stat(d)
	if err != nil {
		glog.Error(err)
	}

	b := finfo.IsDir()
	if !b {
		return false
	}
	return true
}

func setOptDenyCurrentBranch(d string) {
	/*
		   	* set receive.denyCurrentBranch to false to allow
			 * push to non-bare repository. Cause destination
			 * workspace to not be update until
			 * git reset --hard is run.
			 * TODO: figure out how to do this better
	*/
	f, err := os.OpenFile(d+"/.git/config", os.O_RDWR, 0644)
	if err != nil {
		glog.Fatal(err)
	}
	defer f.Close()
	_, err = f.Seek(0, os.SEEK_END)
	if err != nil {
		glog.Fatal(err)
	}
	n, err := f.WriteString("[receive]\ndenyCurrentBranch = warn")
	if err != nil || n < 1 {
		glog.Fatal(err)
	}

}
